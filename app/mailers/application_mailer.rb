class ApplicationMailer < ActionMailer::Base
  default from: (ENV['EMAIL_FROM'] || "noreply@example.com")
  layout 'mailer'
end
